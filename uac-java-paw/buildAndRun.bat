@echo off
call mvn clean package
call docker build -t com.mycompany/uac-java-paw .
call docker rm -f uac-java-paw
call docker run -d -p 9080:9080 -p 9443:9443 --name uac-java-paw com.mycompany/uac-java-paw