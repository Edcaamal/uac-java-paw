#!/bin/sh
mvn clean package && docker build -t com.mycompany/uac-java-paw .
docker rm -f uac-java-paw || true && docker run -d -p 9080:9080 -p 9443:9443 --name uac-java-paw com.mycompany/uac-java-paw